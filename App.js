import React from 'react';
import {Provider} from 'react-redux';
import store from './src/redux';
import {MainNavigation} from './src/navigations/main_navigation';

const App = () => {
  return (
    <Provider store={store}>
      <MainNavigation />
    </Provider>
  );
};

export default App;
