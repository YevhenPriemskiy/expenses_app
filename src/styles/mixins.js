import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {Dimensions} from 'react-native';
export function rs(size, direction = 'w', baseWidth = 375, baseHeight = 812) {
  if (direction === 'w') {
    return wp((size / baseWidth) * 100);
  } else if (direction === 'h') {
    return hp((size / baseHeight) * 100);
  }
}
export function fullWidth() {
  return Dimensions.get('window').width;
}
export function fullHeight() {
  return Dimensions.get('window').height;
}

export function isSmall(size = 325) {
  return fullWidth() <= size;
  // || fullHeight() <= 667
}
