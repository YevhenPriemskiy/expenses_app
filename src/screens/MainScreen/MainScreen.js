import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Mixins} from '../../styles';
import LinearGradient from 'react-native-linear-gradient';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import Chart from '../../components/Chart';
import Divider from '../../components/Divider';
import ScanButton from '../../components/ScanButton';
import Card from '../../components/Card';
import AmountButton from '../../components/AmountButton/AmountButton';
import BottomComponent from '../../components/BottomComponent';
import {BG_GRADIENT_COLORS} from '../../constants/App';
const MainScreen = () => {
  const insetsTop = useSafeAreaInsets().top;
  return (
    <View style={styles.root}>
      <LinearGradient
        colors={BG_GRADIENT_COLORS}
        style={styles.gradientContainer(insetsTop)}>
        <Chart />
        <Divider />
        <ScanButton />
        <Card />
        <AmountButton />
        <BottomComponent />
      </LinearGradient>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  gradientContainer: insetsTop => ({
    flex: 1,
    paddingTop: Mixins.isSmall() ? Mixins.rs(20) : Mixins.rs(insetsTop + 30),
  }),
});

export default React.memo(MainScreen);
