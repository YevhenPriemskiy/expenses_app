import * as CHART_ACTION_CREATORS from './chartActionCreators';

export default {
  ...CHART_ACTION_CREATORS,
};
