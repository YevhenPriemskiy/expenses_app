import CHART_ACTIONS from '../actions/chartActions';

export const setChartData = payload => ({
  type: CHART_ACTIONS.SET_CHART_DATA,
  payload,
});
