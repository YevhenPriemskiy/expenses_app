import {combineReducers, createStore} from 'redux';
import {chartReducer} from './reducers/chart';

const rootReducer = combineReducers({
  chart: chartReducer,
});

const store = createStore(rootReducer);

export default store;
