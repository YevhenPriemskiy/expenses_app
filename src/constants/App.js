export const BUTTON_TEXT = 'AMOUNT';
export const BG_GRADIENT_COLORS = ['#EBF3FA', '#DDE7F3', '#E5F0F9'];
export const CARD_GRADIENT_COLORS = [
  'rgba(255, 255, 255, 0.3)',
  'rgba(87, 21, 172, 0.01)',
  'rgba(30, 141, 255, 0.15)',
  'rgba(255, 255, 255, 0.21)',
];
