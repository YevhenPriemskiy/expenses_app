import {Mixins} from '../styles';

export const CALLBACK_THRESHOLD = 0.5;
export const SNAP_POINTS = [Mixins.rs(50), Mixins.fullHeight() * 0.8];
export const GRADIENT_COLORS = ['#FFFFFF', '#E3EDF7', '#FFFFFF'];
