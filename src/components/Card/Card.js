import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {Mixins} from '../../styles';
import MasterCardLogo from '../../assets/icons/MASTERCARD.svg';
import LinearGradient from 'react-native-linear-gradient';
import {CARD_GRADIENT_COLORS} from '../../constants/App';
import CardIcon from '../../assets/icons/CARD.svg';

const Card = () => {
  return (
    <View style={styles.root}>
      <CardIcon />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    alignSelf: 'center',
    marginTop: Mixins.rs(50, 'h'),
  },
  gradientContainer: {
    width: Mixins.rs(174),
    height: Mixins.rs(100),
    borderRadius: Mixins.rs(12),
  },
  container: {
    width: '100%',
    height: '100%',
  },
  currencyContainer: {
    alignSelf: 'flex-end',
    marginTop: Mixins.rs(10),
    marginRight: Mixins.rs(10),
  },
  currency: {
    fontWeight: '600',
  },
  logoContainer: {
    alignSelf: 'flex-end',
    justifyContent: 'flex-end',
    marginBottom: Mixins.rs(12),
    position: 'absolute',
    bottom: 0,
    right: Mixins.rs(12),
  },
});

export default React.memo(Card);
