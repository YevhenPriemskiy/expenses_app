import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Neomorph} from 'react-native-neomorph-shadows';
import {Mixins} from '../../styles';
import ChartIcon from '../../assets/icons/CHART.svg';

const Chart = () => {
  return (
    <View style={styles.root}>
      <Neomorph
        useArt
        inner={false}
        swapShadows={true}
        darkShadowColor={'#8A9BBD'}
        style={styles.container}>
        <View style={styles.iconContainer}>
          <ChartIcon />
        </View>
      </Neomorph>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    alignSelf: 'center',
    paddingBottom: 20,
  },
  container: {
    shadowRadius: 6,
    shadowOpacity: 0.3,
    borderRadius: 40,
    backgroundColor: '#E9F0F7',
    width: 130,
    height: Mixins.rs(Mixins.isSmall() ? 60 : 50, 'h'),
    marginTop: 20,
    borderColor: '#D6E5F2',
    borderWidth: 0.3,
    overflow: 'hidden',
  },
  iconContainer: {
    alignSelf: 'center',
    justifyContent: 'center',
    height: Mixins.rs(Mixins.isSmall() ? 60 : 50, 'h'),
    alignItems: 'center',
    width: '100%',
    borderRadius: 40,
  },
});

export default React.memo(Chart);
