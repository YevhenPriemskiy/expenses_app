import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Neomorph} from 'react-native-neomorph-shadows';
import {Mixins} from '../../styles';

const Divider = () => {
  return (
    <View style={styles.root}>
      <Neomorph
        inner={true}
        swapShadows={false}
        style={styles.divider}
        darkShadowColor={'#8A9BBD'}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    width: '95%',
    alignSelf: 'center',
    opacity: 0.6,
  },
  divider: {
    shadowRadius: 3,
    borderRadius: 15,
    shadowOpacity: 1,
    backgroundColor: '#E9F0F7',
    width: Mixins.fullWidth() * 0.95,
    height: 4,
    marginTop: 20,
    borderWidth: 0.1,
    borderColor: '#E9F0F7',
  },
});

export default React.memo(Divider);
