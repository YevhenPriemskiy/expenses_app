import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {Mixins} from '../../styles';
import MasterCardLogo from '../../assets/icons/MASTERCARD.svg';

const Info = () => {
  return (
    <View style={styles.root}>
      <View style={styles.textContainer}>
        <Text>Some Text</Text>
      </View>
      <View style={styles.logoContainer}>
        <MasterCardLogo />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    width: '85%',
    marginTop: Mixins.rs(40, 'h'),
    justifyContent: 'space-evenly',
  },
  textContainer: {
    width: '50%',
  },
  logoContainer: {
    width: '20%',
    alignItems: 'center',
  },
});

export default React.memo(Info);
