import React from 'react';
import {TextInput, TouchableOpacity, View, StyleSheet} from 'react-native';
import Close from '../../assets/icons/Close.svg';
import Search from '../../assets/icons/Search.svg';
import {Mixins} from '../../styles';

const SearchInput = ({handleClose = () => {}}) => {
  return (
    <View style={styles.root}>
      <View style={styles.inputContainer}>
        <Search style={styles.input} />
        <TextInput placeholder={'Search'} style={{width: '90%'}} />
      </View>
      <View style={styles.close}>
        <TouchableOpacity onPress={handleClose}>
          <Close />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    width: '85%',
  },
  close: {
    alignSelf: 'center',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    width: '10%',
  },
  inputContainer: {
    flexDirection: 'row',
    width: '90%',
    alignItems: 'center',
  },
  input: {
    marginRight: Mixins.rs(20),
  },
});

export default React.memo(SearchInput);
