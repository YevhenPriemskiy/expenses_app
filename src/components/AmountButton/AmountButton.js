import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {Mixins} from '../../styles';
import {Neomorph} from 'react-native-neomorph-shadows';
import {BUTTON_TEXT} from '../../constants/App';

const AmountButton = () => {
  return (
    <View style={styles.root}>
      <Neomorph
        inner={true}
        swapShadows={false}
        darkShadowColor={'#88A5BF'}
        style={styles.container}>
        <View style={styles.textContainer}>
          <Text style={styles.text}>{BUTTON_TEXT}</Text>
        </View>
      </Neomorph>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    alignSelf: 'center',
    marginTop: Mixins.rs(30, 'h'),
  },
  container: {
    shadowRadius: 4,
    shadowOpacity: 0.7,
    borderRadius: 15,
    backgroundColor: '#E3EDF7',
    width: Mixins.fullWidth() * 0.9,
    height: 50,
    marginTop: 20,
  },
  textContainer: {
    alignSelf: 'center',
    justifyContent: 'center',
    height: 50,
  },
  text: {
    color: 'rgba(42, 58, 89, 0.4)',
    fontSize: 12,
  },
});

export default React.memo(AmountButton);
