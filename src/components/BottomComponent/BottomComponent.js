import React from 'react';
import BottomSheet from 'reanimated-bottom-sheet';
import {Mixins} from '../../styles';
import {
  LayoutAnimation,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
  UIManager,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import SearchInput from '../BottomSheetUtils/Search';
import Info from '../BottomSheetUtils/Info';
import {
  CALLBACK_THRESHOLD,
  GRADIENT_COLORS,
  SNAP_POINTS,
} from '../../constants/BottomComponent';
import {isAndroid} from '../../utils/isAndroid';

if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}
const Container = ({children}) => {
  return isAndroid() ? (
    <LinearGradient
      colors={GRADIENT_COLORS}
      start={{x: -3, y: 0}}
      end={{x: 3, y: 0}}
      style={styles.gradient}>
      {children}
    </LinearGradient>
  ) : (
    <LinearGradient
      colors={GRADIENT_COLORS}
      start={{x: -3, y: 0}}
      locations={[0, 0.1, 0]}
      end={{x: 3, y: 0}}
      style={styles.gradient}>
      {children}
    </LinearGradient>
  );
};

const BottomComponent = () => {
  const [snapped, setSnapped] = React.useState(false);
  const bottomSheetRef = React.useRef();

  const handleOpenStart = React.useCallback(() => {
    LayoutAnimation.easeInEaseOut();
    setSnapped(true);
  }, []);

  const handleCloseStart = React.useCallback(() => {
    LayoutAnimation.easeInEaseOut();
    setSnapped(false);
  }, []);

  const handleModalClose = React.useCallback(() => {
    bottomSheetRef.current?.snapTo(0);
  }, []);

  const renderContent = React.useCallback(() => {
    return (
      <View style={styles.root}>
        <Container>
          {snapped && (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              <View style={styles.bottomContentContainer}>
                <SearchInput handleClose={handleModalClose} />
                <Info />
              </View>
            </TouchableWithoutFeedback>
          )}
        </Container>
      </View>
    );
  }, [snapped]);

  return (
    <BottomSheet
      callbackThreshold={CALLBACK_THRESHOLD}
      enabledInnerScrolling={false}
      ref={bottomSheetRef}
      snapPoints={SNAP_POINTS}
      onOpenStart={handleOpenStart}
      onCloseStart={handleCloseStart}
      renderContent={renderContent}
    />
  );
};

const styles = StyleSheet.create({
  root: {
    alignSelf: 'center',
    shadowColor: '#88A5BF',
    shadowOpacity: 0.1,
    shadowRadius: 10,
    elevation: 2,
  },
  gradient: {
    width: Mixins.fullWidth(),
    height: Mixins.fullHeight(),
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 20,
  },
  bottomContentContainer: {
    width: Mixins.fullWidth() * 0.9,
    alignSelf: 'center',
    alignItems: 'center',
    height: '100%',
    marginTop: Mixins.rs(20, 'h'),
  },
});

export default React.memo(BottomComponent);
