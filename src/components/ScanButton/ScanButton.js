import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Mixins} from '../../styles';
import {Neomorph} from 'react-native-neomorph-shadows';
import QR from '../../assets/icons/QR.svg';

const ScanButton = () => {
  return (
    <View style={styles.root}>
      <Neomorph
        useArt
        inner={false}
        swapShadows={true}
        lightShadowColor={'#E3EDF7'}
        style={styles.container}>
        <View style={styles.qrContainer}>
          <QR />
        </View>
      </Neomorph>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    marginLeft: Mixins.rs(30),
    marginTop: Mixins.rs(25, 'h'),
  },
  container: {
    shadowRadius: 5,
    borderRadius: 16,
    backgroundColor: '#E3EDF7',
    width: Mixins.rs(55, 'h'),
    height: Mixins.rs(55, 'h'),
    marginTop: 20,
  },
  qrContainer: {
    alignSelf: 'center',
    justifyContent: 'center',
    height: Mixins.rs(55, 'h'),
  },
});

export default React.memo(ScanButton);
