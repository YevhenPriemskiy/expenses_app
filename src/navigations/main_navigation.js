import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import MainScreen from '../screens/MainScreen';

const Stack = createStackNavigator();

export const MainNavigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={'Main'}
        screenOptions={{headerShown: false}}>
        <Stack.Screen name={'Main'} component={MainScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
