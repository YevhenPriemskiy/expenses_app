# Expenses App

## Contents
1. [Features](#features)
2. [Screenshots](#screenshots)
3. [Installation](#installation)
4. [License](#license)


### Features
All the given tasks completed:
 - Neumorphic design
 - Fully animated popup
 - APK   

App works both on Android and IOS devices,you can test it out by downloading apk or running on emulators.Guide given on Installation section!



### Screenshots
![ScreenShot](assets/mainScreen.png)
![ScreenShot](assets/PopUp.png)

### Installation

Install the dependencies and start the app.

```sh
npm i
cd ios && pod install
cd ..
npm run ios
npm run android
```
or 
 ```
 You can launch apk file on android device,to get the apk just go to assets/test.apk and download it
```

### License

MIT
